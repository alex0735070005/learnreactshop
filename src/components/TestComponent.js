import React, {Component} from 'react';

class TestComponent extends Component {

    shouldComponentUpdate(nextProps) {
        console.log('shouldComponentUpdate');
        return nextProps.message.length > 10
    }

    render() {

        const {
            message,
        } = this.props;
        
        return (
            <h2>
                Hello {message}
            </h2>
        )
    }
}

export default TestComponent;