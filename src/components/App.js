import React, { Component } from 'react';
// import Header from './main/Header';
import TestComponent from './TestComponent';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      valueInput: 'vasa',
      list:[
        {name:'field-1', active:false}, 
        {name:'field-2', active:true},
        {name:'field-3', active:false},
      ]
    }
    console.log('1) CONSTRUCTOR');
  }

  static getDerivedStateFromProps(props, state) {
    console.log('2) getDerived');
    return null;
  }

  componentDidMount() {
    console.log('4) DIDMOUNT');
  }

 

  onChangeInput = (event) => {
    this.setState({
      valueInput:event.target.value
    })
  }

  render() {
    const {
      valueInput,
    } = this.state;

    console.log('3) RENDER');
    return (
      <div className="App">
        <input 
          onChange = {this.onChangeInput}
          value = {valueInput}
        />
        <TestComponent
          message = {valueInput}
        />
      </div>
    );
  }
}

export default App;
